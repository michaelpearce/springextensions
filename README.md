# Welcome to Spring Extensions by Sourcery.IT

## What is Spring Extensions?

Spring Extensions are a collection of modules used to extend the spring functionality.
All the extensions are open source under the apache license. Please feel free to contribute back fix's and feature addtions.


# Dependencies:

 * Spring 3.2
 * Java 6

